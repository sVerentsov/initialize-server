# install packages 
apt update
apt install -y git vim tmux software-properties-common ufw net-tools htop
add-apt-repository -y ppa:deadsnakes/ppa
apt install -y python3.10-dev
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.10 get-pip.py
python3.10 -m pip install rmate

# install oh-my-zsh

apt install -y zsh
chsh -s /usr/bin/zsh root
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
sed -i 's/DSHELL=\/bin\/bash/DSHELL=\/bin\/zsh/' /etc/adduser.conf
sed -i 's/SHELL=\/bin\/sh/SHELL=\/bin\/zsh/' /etc/default/useradd
sed -i 's/\/root\/.oh-my-zsh/$HOME\/.oh-my-zsh/' /root/.zshrc
cp -r /root/.oh-my-zsh /etc/skel/
cp /root/.zshrc /etc/skel

#Change ssh port
sed -i 's/#Port 22/Port 2763/g' /etc/ssh/sshd_config

# Disable SSH root login
sed -i "s/#PermitRootLogin prohibit-password/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/PermitRootLogin prohibit-password/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin Yes/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin YES/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i 's/LoginGraceTime .*/LoginGraceTime 60/g' /etc/ssh/sshd_config
sed -i 's/PermitRootLogin .*/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -i 's/Protocol .*/Protocol 2/g' /etc/ssh/sshd_config
sed -i 's/#PermitEmptyPasswords .*/PermitEmptyPasswords no/g' /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication .*/PasswordAuthentication no/g' /etc/ssh/sshd_config
sed -i 's/X11Forwarding .*/X11Forwarding no/g' /etc/ssh/sshd_config
service sshd restart

# setup ufw 
ufw allow 2763
ufw --force enable

# add Verentsov
useradd -m verentsov
usermod -aG sudo verentsov
su - verentsov -c "umask 022 ; mkdir .ssh ; wget  -O .ssh/authorized_keys https://gist.githubusercontent.com/sVerentsov/c460c62295d1a33108d74420921e56d1/raw/e490ed6943eb69b71a3fd9088969d9b643d89585/authorized_keys"
#prompt user to create password on first login
passwd -de verentsov

