curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install -y gitlab-runner

sudo systemctl disable gitlab-runner
sudo systemctl stop gitlab-runner

gitlab-runner register -n --url https://gitlab.com/ --registration-token $@ --executor shell --description $(hostname)

sudo tmux new -s gitlab-runner -d gitlab-runner run